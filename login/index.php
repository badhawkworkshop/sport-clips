<!doctype html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="You deserve an MVP experience from a stylist who is an expert in haircuts for men and boys. Visit a Sport Clips near you in 2018 to relax and watch the game.">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

<!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

<link rel="stylesheet" href="../css/main.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<title>Edit Location Data</title>

</head>
    <body class="login">

      <span>
        <form>
          <input name="user" id="user" placeholder="username" type="text" />
          <input name="pass" id="pass" placeholder="password" type="password" />
          <p class="errors"></p>
          <div class="submit btn">Submit</div>
        </form>
      </span>

  


    <!-- FIREBASE -->
    <script src="https://www.gstatic.com/firebasejs/5.0.4/firebase.js"></script>

    <!-- JQUERY -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- CUSTOM -->
    <script src="../js/libs.min.js"></script>
    <script src="../js/main.min.js"></script>

    </body>
</html>
