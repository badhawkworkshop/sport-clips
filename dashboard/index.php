<!doctype html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="You deserve an MVP experience from a stylist who is an expert in haircuts for men and boys. Visit a Sport Clips near you in 2018 to relax and watch the game.">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

<!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

<link rel="stylesheet" href="../css/main.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<title>Edit Location Data</title>

</head>
    <body class="dashboard">

      <div class="top-bar">
        <p class="username">Logged in as: <span></span></p>
        <div class="logout">Logout</div>
      </div>

      
      <div class="menu">
        <p>Please select an option:</p>
        <span>
          <div class="add-item btn">Add a New Location</div>
          <div class="edit-items btn">Edit a location</div>
          <div class="total-items btn">Show Submission Totals</div>
          <div class="export btn">Export Locations to CSV</div>
        </span>
      </div>

      
      <section class="edit">
        <div class="app-header">
          <p>Edit a Location</p>
          <div class="close"><i class="fas fa-times"></i></div>
        </div>
        <div class="list">
          <select>
            <option selected disabled>Select a Location</option>
          </select>
        </div>
        <form>
          <span class="full">
            <label>Title</label>
            <input type="text" id="title"/>
          </span>
          <span>
            <label>Address</label>
            <input type="text" id="address"/>
          </span>
          <span>
            <label>Address Line 2</label>
            <input type="text" id="address2" />
          </span>
          <span>
            <label>City</label>
            <input type="text" id="city" />
          </span>
          <span class="small">
          <label>State</label>
            <input type="text" id="state" />
            </span>
          <span class="small">
            <label>Zip</label>
            <input type="text" id="zip" />
          </span>
          <span>
            <label>Cross Streets/Description</label>
            <input type="text" id="extra" />
          </span>
          <span>
            <label>Phone</label>
            <input type="text" id="phone" />
          </span>
          <span>
            <label>Website</label>
            <input type="text" id="website" />
          </span>
          <span class="small">
            <label>Budget</label>
            <input type="text" id="budget" />
          </span>
          <span class="small">
            <label>What Converts Tracking ID</label>
            <input type="text" id="leadTrackingScriptID" />
          </span>
          <span class="small">
            <label>Latitude</label>
            <input type="text" id="latitude" />
          </span>
          <span class="small">
            <label>Longitude</label>
            <input type="text" id="longitude" />
          </span>
          <span class="small">
            <label>Location is On</label>
            <input type="text" id="locationOn" />
          </span>
          <!-- <span class="small">
            <label>Monthly Spent on Facebook</label>
            <input type="text" id="montlySpentFacebook" />
          </span> -->
          <!-- <span class="small">
            <label>Monthly Spent on Google</label>
            <input type="text" id="montlySpentGoogle" />
          </span> -->
          <!-- <span class="small">
            <label>Budget Remaining</label>
            <input type="text" id="budgetRemaining" />
          </span> -->
          <span class="small">
            <label>Total Submissions</label>
            <input type="text" id="totalSubmissions" />
          </span>
          <!-- <span class="small">
            <label>Total Cost Per Form Submission</label>
            <input type="text" id="TotalCostPerFormSubmission" />
          </span> -->
          <div class="btn" id="btn-edit-location">Save Location</div>
        </form>
        
        
      </section>



      <section class="add">
        <div class="app-header">
          <p>Add a Location</p>
          <div class="close"><i class="fas fa-times"></i></div>
        </div>
        <form>
          <span class="full">
            <label>Store Name</label>
            <input type="text" id="title"  />
          </span>
          <span>
            <label>Address Line 1</label>
            <input type="text" id="address"/>
          </span>
          <span>
            <label>Address Line 2</label>
            <input type="text" id="address2" />
          </span>
          <span>
            <label>City</label>
            <input type="text" id="city" />
          </span>
          <span class="small">
            <label>State</label>
            <input type="text" id="state" />
          </span>
          <span  class="small">
            <label>Zip</label>
            <input type="text" id="zip" />
          </span>
          <span>
            <label>Cross Streets/Description</label>
            <input type="text" id="extra" />
          </span>
          <span>
            <label>Phone</label>
            <input type="text" id="phone" />
          </span>
          <span>
            <label>Website</label>
            <input type="text" id="website" />
          </span>
          <span>
            <label>Budget</label>
            <input type="text" id="budget" />
          </span>
          <span class="small">
            <label>Latitude</label>
            <input type="text" id="latitude" />
          </span>
          <span class="small">
            <label>Longitude</label>
            <input type="text" id="longitude" />
          </span>
          <span class="small">
            <label>What Converts Tracking ID</label>
            <input type="text" id="leadTrackingScriptID" />
          </span>
          <div class="btn" id="btn-add-location">Add Location</div>
        </form>
      </section>
        
        
      <section class="totals">
        <div class="app-header">
          <p>Submissions</p>
          <div class="close"><i class="fas fa-times"></i></div>
        </div>

        <div class="totals-menu">
          <div class="totals-by-date btn">By Date</div>
          <div class="totals-by-location btn">By Location</div>
        </div>

        <p class="forAllTime">Total Submissions: <span></span></p>

        <div class="by-date">
          <p class="forTimeSelected">Total Submissions for Time Selected: <span></span></p>
          <select class="select-year">
            <option selected disabled>Select a Year</option>
            <option value="2018">2018</option>
            <option value="2019">2019</option>
            <option value="2020">2020</option>
          </select>
          <select class="select-month">
            <option selected disabled>Select a Month</option>
            <option value="0">January</option>
            <option value="1">February</option>
            <option value="2">March</option>
            <option value="3">April</option>
            <option value="4">May</option>
            <option value="5">June</option>
            <option value="6">July</option>
            <option value="7">August</option>
            <option value="8">September</option>
            <option value="9">October</option>
            <option value="10">November</option>
            <option value="11">December</option>
          </select>
          <div class="btn" id="btn-show-submissions">Show Submissions</div>
        </div>

        <div class="by-location">
          <p class="forLocationsSelected">Total Submissions for Location Selected: <span></span></p>
          <select>
            <option selected disabled>Select a Location</option>
          </select>
        </div>

        <div class="submission-list"></div>
        
      </section>


    <!-- FIREBASE -->
    <script src="https://www.gstatic.com/firebasejs/5.0.4/firebase.js"></script>

    <!-- JQUERY -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- CUSTOM -->
    <script src="../js/libs.min.js"></script>
    <script src="../js/main.min.js"></script>

    </body>
</html>
