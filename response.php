<?php
//form data: Contact Information
$email = $_POST['email'];
$phone = $_POST['phone'];
$loctitle = $_POST['loctitle'];
$locaddress = $_POST['locaddress'];
$loccity = $_POST['loccity'];
$locphone = $_POST['locphone'];
$locwebsite = $_POST['locwebsite'];
$locltsid = $_POST['locltsid'];


$fromemail = "no_reply@sportclips.com";
$emailsubject = "Your Sports Clips Coupon!";
$redirectsuccess = "/thank-you/?ic=" . $locltsid;
$redirectfailed = "https://sportclips.com";

// Make sure the client is using a browser (to avoid spam bots)
if (!isset($_SERVER['HTTP_USER_AGENT'])) {
  die("Forbidden - You are not authorized to view this page");
  exit;
}
// Make sure the form was POSTed (to avoid spam bots and hackers)
if (!$_SERVER['REQUEST_METHOD'] == "POST") {
  die("Forbidden - You are not authorized to view this page");
  exit;
}
// Attempt to defend against header injections:
$badStrings = array("Content-Type:",
                     "MIME-Version:",
                     "Content-Transfer-Encoding:",
                     "bcc:",
                     "cc:");

// Loop through each POST'ed value and test if it contains
// one of the $badStrings:
foreach($_POST as $k => $v){
   foreach($badStrings as $v2){
       if(strpos($v, $v2) !== false){
           logBadRequest();
           header("HTTP/1.0 403 Forbidden");
               exit;
       }
   }
}

// Form variables

// Contact information
//sumbission data
$ipaddress = $_SERVER['REMOTE_ADDR'];
$dateSent = date('d/m/Y');
$time = date('H:i:s');


// Build email message
$message = "
Hey you have a coupon!
        
        
Your contact info
_____________________
Email: {$email}
Phone Number: {$phone}


The location that was selected
_____________________
{$loctitle}
Address: {$locaddress} {$loccity}
Phone: {$phone}
Website: {$locwebsite}
        
      
      
This message was sent on {$dateSent} at {$time}
from the ip address:  {$ipaddress} ";
        
// send mail
if ($email !== "") {
  $response = mail($email, $emailsubject, $message, "From:$fromemail");
  // redirect
  if($response == 1) {
    header("Location: " . $redirectsuccess );
  }
}
else {
  header("Location: " . $redirectfailed );
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Oh No! Something went wrong.</title>
</head>
<body>
<p align="center">An error has occurred while processing the mail request.</p>
<p align="center"> Please try again, or contact <?php echo $contactemail_1; ?></p>
</body>

</html>

