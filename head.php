<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="description" content="You deserve an MVP experience from a stylist who is an expert in haircuts for men and boys. Visit a Sport Clips near you in 2018 to relax and watch the game.">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        

        <meta property="og:title" content="Site Title"/>
        <meta property="og:url" content="http://url.com"/>
        <meta property="og:site_name" content="Site Title"/>
        <meta property="og:image" content=""/>
        <meta property="og:description" content=""/>

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

        <link rel="stylesheet" href="/css/main.css">
        <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

