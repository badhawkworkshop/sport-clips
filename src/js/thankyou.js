// thank you page scripts
$(document).ready(function(){ 
//only fire on thankyou page
if ( $("body").hasClass('page-thank-you') ){ 

  //countdown timer
  var redirectURL = sessionStorage.getItem('tyrd');
  if(!redirectURL){
    //error fallback
    redirectURL = "https://sportclips.com/";
  }

  var timeleft = 10;
  var countdownTimer = setInterval(function(){
    document.getElementById("seconds").value = 10 - --timeleft;
    if(timeleft <= 0){
      clearInterval(countdownTimer);
      //redirect after 10 seconds
      window.location.href = redirectURL;
    }
    $('#seconds').text(timeleft);
  },1000);



}
}); //end dr