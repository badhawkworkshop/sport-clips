/*
  cybermark = AIzaSyAijpZQTQnnDkgu5lQ9raHqqBlOeEcA3AY
*/

if ( $("body").hasClass('page-map')  ){
// INITALIZING MAP -----------------------
// this is outside of document.ready because of the async loading of the google map

// setting global map, closest location and info windows
var map, 
    infoWindow, 
    closestLocation,
    closestLocationKM,
    userLocation;



//init map function
function initMap() {

  // create map and center it to the middle of the United States
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 98.5795, lng: 39.8283},
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // initalized the info window
  infoWindow = new google.maps.InfoWindow;

  // First try HTML5 geolocation
  if (navigator.geolocation) {
    
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      //display map
      displayMap(pos, map);
      $('.geo p').css("display", "none");
      $('.geo .btn-locations').css("display", "block");
      
      // uncomment below if you'd like a marker for the user's current position
      // infoWindow.setPosition(pos);
      // infoWindow.setContent('Your Location');
      // infoWindow.open(map);

      
    // if geolocation fails or is blocked
    }, function() {

      //hide areas on the page
      $('.geo').css("display", "none");
      $('#location').css("display", "none");
      $('.zip').css("display", "block");
      // center map to is Lebanon, KS 66952
      getCenterGeoLocation(map);
      //event listener for the zip code lookup button
      $('.btn-zip').on('click', function(){
        var zip = $('#zipcodeLookup').val();
        //only run if the field has data
        if (zip != ""){
          $('.zip').css("display", "none");
          $('#location').css("display", "block");
          getZipCodeGeoLocation(zip, map);
        }
      });
    });

  // geolocation or zip error handler
  } else {
    handleLocationError(false, infoWindow, map.getCenter());
  }
}


// makes a call to Google Geolocation using the center of the united state
function getCenterGeoLocation(map){
  var url = "https://maps.googleapis.com/maps/api/geocode/json?address=66952&key=AIzaSyAijpZQTQnnDkgu5lQ9raHqqBlOeEcA3AY";
      $.getJSON( url, function( data ) { 
        //center the map to the user's current position
        var pos = data.results[0].geometry.location;

        //display map
        map.setCenter(pos);
        userLocation = pos;
        addPinsNoGeo(map);
      });
}

// makes a call to Google Geolocation using either zip, city, state or address
function getZipCodeGeoLocation(zip, map){
  var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + zip + "&key=AIzaSyAijpZQTQnnDkgu5lQ9raHqqBlOeEcA3AY";
      $.getJSON( url, function( data ) { 
        //center the map to the user's current position
        var pos = data.results[0].geometry.location;
        //display map
        displayMap(pos, map);
      });
}

// displays the map and centers it to the user's location
function displayMap(position, map){
  map.setCenter(position);
  userLocation = position;
  addPins(map);
}


// function to handle geo location errors
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
  infoWindow.open(map);
}


// adding pins from the JSON data file
// activeInfoWindow is global so that we can close it later
var activeInfoWindow; 
function addPins(map){

  // getting the data as json from firebase
  var url = "https://sportclips-promo.firebaseio.com/locations/.json";
  $.getJSON( url, function( data ) {

      // move through locations
      $.each( data, function( itemkey, item ) {


        // don't display the item if it's turned off 'locationOn:false'
        if(item.locationOn){
          
          var latLng = new google.maps.LatLng(item.latitude, item.longitude);

          var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: item.title
          });

          // creating the pin info window 
          var infoWindow = new google.maps.InfoWindow();
          // creating the event listener to show info when the item is clicked
          google.maps.event.addListener(marker, "click", function(e) {
            
          // closes the current infoWindow when a new one is opened
          if (activeInfoWindow) { activeInfoWindow.close();}
              //creating a new info window 
              infoWindow.setContent(item.title);
              // opening the infoWindow
              infoWindow.open(map, marker);
              //update location area with clicked location data
              updateLocationArea(item, itemkey);
              // setting the activeInfoWindow so it can be closed later
              activeInfoWindow = infoWindow;
          });

          // finding distance from the user's location
          var distance = getDistanceFromLatLong(userLocation.lat,userLocation.lng,item.latitude,item.longitude);
          if (closestLocationKM == undefined){
            closestLocationKM = distance;
            closestLocation = item;
            updateLocationArea(closestLocation, itemkey);
          }
          else if(distance < closestLocationKM){
            closestLocationKM = distance;
            closestLocation = item;
            updateLocationArea(closestLocation, itemkey);
          }
          
        }
    });

});

}



// adding pins from the JSON data file
// activeInfoWindow is global so that we can close it later 
function addPinsNoGeo(map){

  // getting the data as json from firebase
  var url = "https://sportclips-promo.firebaseio.com/locations/.json";
  $.getJSON( url, function( data ) {

      // move through locations
      $.each( data, function( itemkey, item ) {


        // don't display the item if it's turned off 'locationOn:false'
        if(item.locationOn){
          
          var latLng = new google.maps.LatLng(item.latitude, item.longitude);

          var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: item.title
          });

          // creating the pin info window 
          var infoWindow = new google.maps.InfoWindow();
          // creating the event listener to show info when the item is clicked
          google.maps.event.addListener(marker, "click", function(e) {
            
          // closes the current infoWindow when a new one is opened
          if (activeInfoWindow) { activeInfoWindow.close();}
              //creating a new info window 
              infoWindow.setContent(item.title);
              // opening the infoWindow
              infoWindow.open(map, marker);
              //update location area with clicked location data
              updateLocationArea(item, itemkey);
              // setting the activeInfoWindow so it can be closed later
              activeInfoWindow = infoWindow;
          });

          //create list of all pins
          $('.geoBlockedLocationList').append('<div class="location ' + itemkey + '"><h4>' + item.title + '</h4><p class="address">' + item.address + '</p><p><span class="city">' + item.city + '</span>, <span class="state">' + item.state + '</span> <span class="zip">' + item.zip + '</span></p><button class="btn-select" data-zip=' + item.zip + ' data-key=' + itemkey + '>Select Location</button></div>');
          
        }
    });

});

//add the event listener for the new buttons
$(document).on("click", ".btn-select", function(){
  $('.zip').css("display", "none");
  $('#location').css("display", "block");
  var zip = $(this).attr('data-zip');
  var key = $(this).attr('data-key');
  var title = $('.'+key).find('h4').text();
  var address = $('.'+key + " .address").text();
  var city = $('.'+key + " span.city").text();
  var state = $('.'+key + " span.state").text();
  var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + zip + "&key=AIzaSyDjh-yIOL6Uj_uKMSCyq3gbtisT2JgbwSY";
      $.getJSON( url, function( data ){ 
        //center the map to the user's current position which is the new zip code
        var pos = data.results[0].geometry.location;
        //display map
        map.setCenter(pos);
        userLocation = pos;
        $('#location').css("opacity", 1);
        $('#location').attr('data-id', key);
        $('#location .title').text(title);
        $('#location .address').text(address);
        $('#location .city').text(city);
        $('#location .state').text(state);
        $('#location .zip').text(zip);
      });
});

}


// find the closest location
function getDistanceFromLatLong(lat1,lon1,lat2,lon2){
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2); 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}
function deg2rad(deg){
  return deg * (Math.PI/180);
}


//function to populate side data on click or on load
function updateLocationArea(location, key){
  $('#location').css("opacity", 1);
  $('#location').attr('data-id', key);
  $('#location .title').text(location.title);
  $('#location .address').text(location.address);
  $('#location .city').text(location.city);
  $('#location .state').text(location.state);
  $('#location .zip').text(location.zip);
  
}


} //endif