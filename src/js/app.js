/*

  Sport Clips - Promo Landing Page

  Site Development by Badhawk Workshop
  Developer: Daniel Freeman [@dFree]
  

*/

//map page scripts
if ( $("body").hasClass('page-map')  ){


// Initialize Firebase
var config = {
    apiKey: "AIzaSyAEnKtI32XU67Qlp9Ky2H78EzEcKy87q3s",
    authDomain: "sportclips-promo.firebaseapp.com",
    databaseURL: "https://sportclips-promo.firebaseio.com",
    projectId: "sportclips-promo",
    storageBucket: "sportclips-promo.appspot.com",
    messagingSenderId: "875974693306"
  };
  firebase.initializeApp(config);;



// scripts to load once the page loads
$(document).ready(function(){ 


  $('#btn-submit').on('click', function(event){
    event.preventDefault();
    var currentLocationKey = $('#location').data('id');
    var email = $('#form input[name=email]').val();
    var phone = $('#form input[name=phone]').val();
    
    //get the firebase object for the current location
    var url = "https://sportclips-promo.firebaseio.com/locations/" + currentLocationKey + "/.json"
    $.getJSON( url, function(data){
      //set firebase instance
      var dbRef = firebase.database();
      //update Form Submissions for the Month to Date
      var submissions = data.totalSubmissions;
      submissions++;
      data.totalSubmissions = submissions;

      // add this to the submission object
      var currentdate = new Date(); 
      var now = currentdate.getTime();
      var submissionObj = [{
        "time": now, 
        "locationID": currentLocationKey,
        "locationName": data.title,
        "email": email, 
        "phone": phone
      }];
      var submissionsRef = dbRef.ref('submissions');
      dbRef.ref('/submissions/').push(submissionObj);

      //update Firebase with new data
      var locationsRef = dbRef.ref('locations');
      dbRef.ref('/locations/' + currentLocationKey + "/").update({
        "totalSubmissions": data.totalSubmissions
      });

    //add data to hidden inputs
    var citystatezip = (data.city + ", " + data.state + " " + data.zip);
    $('#form input[name=loctitle]').val(data.title);
    $('#form input[name=locaddress]').val(data.address);
    $('#form input[name=loccity]').val(citystatezip);
    $('#form input[name=locphone]').val(data.phone);
    $('#form input[name=locwebsite]').val(data.website);
    $('#form input[name=locltsid]').val(data.leadTrackingScriptID);


    }).done(function(data) {

        //pass data over to the thank-you page
        sessionStorage.setItem('tyrd', data.website);
        sessionStorage.setItem('whatcon', data.leadTrackingScriptID);
        
        //send the email
        //document.forms["form"].submit();

    });

  });



//tag!!
console.log('--------------------------------------');
console.log('-=   built by badhawkworkshop.com   =-');
console.log('--------------------------------------');

});//end document ready


}