// edit a locations data

$(document).ready(function(){ 

if ( $("body").hasClass('dashboard')  ){

// Initialize Firebase
var config = {
    apiKey: "AIzaSyAEnKtI32XU67Qlp9Ky2H78EzEcKy87q3s",
    authDomain: "sportclips-promo.firebaseapp.com",
    databaseURL: "https://sportclips-promo.firebaseio.com",
    projectId: "sportclips-promo",
    storageBucket: "sportclips-promo.appspot.com",
    messagingSenderId: "875974693306"
  };
  firebase.initializeApp(config); 


//check if logged in
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.
    $('.dashboard').fadeIn(300);
    var email = user.email;
    $('.username span').text(user.email);
  } else {
    window.location.href = '/login';
  }
});

//logout
$('.logout').on('click', function(){
  firebase.auth().signOut().then(function() {
    // Sign-out successful.
    window.location.href = '/login';
  }, function(error) {
    // An error happened.
    console.log(error);
  });
});
       


//only run on dashboard
if ( $("body").hasClass('dashboard')  ){


  // app front end state change
  $('.add-item').on('click', function(){
    $('.menu').addClass('menu-hide');
    $('.add').addClass('add-show');
  });

  $('.edit-items').on('click', function(){
    $('.menu').addClass('menu-hide');
    $('.edit').addClass('edit-show');
    createSelect();
  });

  $('.total-items').on('click', function(){
    $('.menu').addClass('menu-hide');
    $('.totals').addClass('totals-show');
    getSubmissionTotal();
    createSubmissionSelect();
  });

  $('.close').on('click', function(){
      $('.menu').removeClass('menu-hide');
      $('.add').removeClass('add-show');
      $('.edit').removeClass('edit-show');
      $('.totals').removeClass('totals-show');
  });

  $('.list select').on('change', function(){
      $('.edit form').fadeIn();
  })



// create the select box for edit a location
function createSelect(){
  //empty select
  $(".list select").empty().append('<option selected disabled>Select a Location</option>');
  //get most recent data
  var url = "https://sportclips-promo.firebaseio.com/locations/.json";
  $.getJSON( url, function( data ) {
        jsonState = data;
        $.each( data, function( itemkey, item ) {
          $('.list select').append("<option value=" + itemkey +">" + item.title + "</option>");
        });
        //select event listener
        $(".list select").on('change', function(){
            var key = this.value;
            populateForm(key);
        });

        $('#btn-edit-location').on('click', function(event){
          event.preventDefault();
          var key = $('.list select').val();
          saveData(key);
          $(this).closest('form').find("input[type=text]").val("");
        });
  });
}


  function populateForm(key){
      var url = "https://sportclips-promo.firebaseio.com/locations/" + key + "/.json"
      $.getJSON( url, function(data){ 
        $('#title').val(data.title);
        $('#address').val(data.address);
        $('#address2').val(data.address2);
        $('#city').val(data.city);
        $('#state').val(data.state);
        $('#zip').val(data.zip);
        $('#extra').val(data.extra);
        $('#phone').val(data.phone);
        $('#budget').val(data.budget);
        $('#latitude').val(data.latitude);
        $('#longitude').val(data.longitude);
        $('#website').val(data.website);
        $('#locationOn').val(data.locationOn);
        // $("#montlySpentFacebook").val(data.montlySpentFacebook);
        // $("#montlySpentGoogle").val(data.montlySpentGoogle);
        // $("#budgetRemaining").val(data.budgetRemaining);
        $("#totalSubmissions").val(data.totalSubmissions);
        // $("#TotalCostPerFormSubmission").val(data.TotalCostPerFormSubmission);
        $("#leadTrackingScriptID").val(data.leadTrackingScriptID);
      });
  }


  function saveData(key){

      var dbRef = firebase.database();
      var locationsRef = dbRef.ref('/locations/' + key);

      locationsRef.update({
        "title": $("#title").val(),
        "latitude": $("#latitude").val(),
        "longitude": $("#longitude").val(),
        "address": $("#address").val(),
        "address2": $("#address2").val(),
        "city": $("#city").val(),
        "state": $("#state").val(),
        "zip": $("#zip").val(),
        "extra": $("#extra").val(),
        "phone": $("#phone").val(),
        "website": $("#website").val(),
        "budget": $("#budget").val(),
        // "montlySpentFacebook": $("#montlySpentFacebook").val(),
        // "montlySpentGoogle": $("#montlySpentGoogle").val(),
        // "budgetRemaining": $("#budgetRemaining").val(),
        "locationOn": $("#locationOn").val(),
        "totalSubmissions": $("#totalSubmissions").val(),
        // "TotalCostPerFormSubmission": $("#TotalCostPerFormSubmission").val(),
        "leadTrackingScriptID": $("#leadTrackingScriptID").val()
      });

  }


}



$('#btn-add-location').on('click', function(event){
  event.preventDefault();

  var dbRef = firebase.database();
  var locationsRef = dbRef.ref('locations');

  locationsRef.push({
    "title": $(".add form #title").val(),
    "latitude": $(".add form #latitude").val(),
    "longitude": $(".add form #longitude").val(),
    "address": $(".add form #address").val(),
    "address2": $(".add form #address2").val(),
    "city": $(".add form #city").val(),
    "state": $(".add form #state").val(),
    "zip": $(".add form #zip").val(),
    "extra": $(".add form #extra").val(),
    "phone": $(".add form #phone").val(),
    "website": $(".add form #website").val(),
    "budget": $(".add form #budget").val(),
    // "montlySpentFacebook": 0,
    // "montlySpentGoogle": 0,
    "budgetRemaining": $(".add form #budget").val(),
    "locationOn": true,
    "totalSubmissions": 0,
    // "TotalCostPerFormSubmission": 0,
    "leadTrackingScriptID": $(".add form #leadTrackingScriptID").val()
  });

  $(this).closest('form').find("input[type=text]").val("");

});


// creates a csv of all location data
$('.export').on('click', function(){
  var url = "https://sportclips-promo.firebaseio.com/locations/.json";
  $.getJSON( url, function( data ) {
    
    //replaces null data with a blank string
    var replacer = function(key, value) { return value === null ? '' : value; } 
    
    //adding header to csv
    var csv = "DB ID, address, address2, budget, city, extra, latitude, leadTrackingScriptID, locationOn, longitude, phone, state, title, totalSubmissions, website, zip";
    csv += "\n";
    
    //create each row
    $.each( data, function( itemkey, item ){
      var row = "";
      row += (itemkey + ",");
      $.each( item, function( itemkey, item ){
        var thing = replacer(itemkey, item);
        row += (thing + ",");
      });
      row = row.substring(',', row.length - 1);
      row += "\n";
      csv += row;
    });
    
    //create the csv file
    var downloadLink = document.createElement("a");
    var blob = new Blob(["\ufeff", csv]);
    var url = URL.createObjectURL(blob);
    downloadLink.href = url;
    downloadLink.download = "data.csv";  //Name the file here
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  });
  
});




//totals menu
$('.totals-by-date').on('click', function(){
  $('.by-location').removeClass('by-location-show');
  $('.by-date').addClass('by-date-show');
  $('.submission-list').html(" ");
});
$('.totals-by-location').on('click', function(){
  $('.by-date').removeClass('by-date-show');
  $('.by-location').addClass('by-location-show');
  $('.submission-list').html(" ");
});
//show submissions on button click
$('#btn-show-submissions').on('click', function(){
  getSubmissionData();
});



// get submission total
function getSubmissionTotal(){
  var url = "https://sportclips-promo.firebaseio.com/submissions/.json";
  $.getJSON( url, function( data ) {
    var totalSubmissions = Object.keys(data).length;
    $('.totals .forAllTime span').text(totalSubmissions);
  });
}


// set submission data
function getSubmissionData(){
  // clear the list
  $('.submission-list').html(" ");
  // data from the date selector
  // month starts at 0. January = 0 
  var selectedMonth = $('.select-month').val();
  var selectedYear = $('.select-year').val();

  var url = "https://sportclips-promo.firebaseio.com/submissions/.json";
  $.getJSON( url, function( data ) {

    var totalSubmissionsForTimeSelected = 0;

    $.each( data, function( itemkey, item ) {
      //get month and year of submission
      var date = new Date(item[0].time);
      var month = date.getMonth();
      var year = date.getFullYear();
      if(selectedYear == year){
        if(selectedMonth == month){
          $('.submission-list').append('<p><span>' + (month+1) + "/" + year + '</span><span>' + item[0].locationName + '</span><span>' + item[0].email + '</span></p>');
          totalSubmissionsForTimeSelected++;
        }
      }
    });
    $('.totals .forTimeSelected span').text(totalSubmissionsForTimeSelected);
  });
}



//create select for submission data
function createSubmissionSelect(){
  //empty select
  $(".by-location select").empty().append('<option selected disabled>Select a Location</option>');
  //get most recent data
  var url = "https://sportclips-promo.firebaseio.com/locations/.json";
  $.getJSON( url, function( data ) {
      jsonState = data;
      $.each( data, function( itemkey, item ) {
        $('.by-location select').append("<option value=" + itemkey +" data-total=" + item.totalSubmissions + ">" + item.title + "</option>");
      });
  });
  //create event listener for the new select
  $(".by-location select").on('change', function(){
    var key = this.value;
    var total = $(this).find(':selected').data('total');
    displaySubmissionByKey(key);
  });
}



//display all submissions for a specific key
function displaySubmissionByKey(key){
  // clear the list
  $('.submission-list').html(" ");
  var counter = 0;
  var url = "https://sportclips-promo.firebaseio.com/submissions/.json";
  $.getJSON( url, function( data ) {
    $.each( data, function( itemkey, item ) {
      if(item[0].locationID == key){
        var date = new Date(item[0].time);
        var month = date.getMonth();
        var year = date.getFullYear();
        $('.submission-list').append('<p><span>' + (month+1) + "/" + year + '</span><span>' + item[0].locationName + '</span><span>' + item[0].email + '</span></p>');
        counter++;
      }
    });
    $('.forLocationsSelected span').text(counter);
  });
}




}


}); // dr