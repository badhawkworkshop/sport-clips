<?php include('../head.php') ?>

<title>Haircuts for Men | Walk-Ins Welcome | Sport Clips 2018</title>

</head>
    <body class="page-thank-you">

      
      <section>
        <h1>Your Coupon is on its way!</h1>
        <p>You will receive your coupon shortly. Please bring your coupon when you come into our barbershop. Thank you!</p>
          
        <ul>
          <li>Non-transferrable and cannot be combined with any other offers</li>
          <li>Expires 30 Days from the day of request</li> 
        </ul>
          
        <p><em>*Please check your spam or junk folder if you do not see your coupon immediately.</em></p>
          
        <p><strong>You will be redirected in <span id="seconds">10</span> seconds to your selected location's website.</strong></p>
      </section>


      


    <!-- JQUERY -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- CUSTOM -->
    <script src="../js/libs.min.js"></script>
    <script src="../js/main.min.js"></script>
    <?php 
      $id = $_GET['ic'];
      echo '<script src="//scripts.iconnode.com/'.$id.'.js"></script>';
    ?>

    </body>
</html>