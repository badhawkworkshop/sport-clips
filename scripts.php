<!-- FIREBASE -->
<script src="https://www.gstatic.com/firebasejs/5.0.4/firebase.js"></script>

<!-- JQUERY -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- CUSTOM -->
<script src="/js/libs.min.js"></script>
<script src="/js/main.min.js"></script>

<!-- GOOGLE MAPS AND GEOLOCATION API -->
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijpZQTQnnDkgu5lQ9raHqqBlOeEcA3AY&callback=initMap">
</script>




