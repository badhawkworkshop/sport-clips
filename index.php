<?php include('head.php') ?>

<title>Haircuts for Men | Walk-Ins Welcome | Sport Clips 2018</title>

</head>
    <body class="page-map">
      
      <section class="top">
        <div><h1>Offer</h1></div>
        <!-- USED FOR A LOADING ICON OR TEXT IF NECESSARY -->
        <span class="geo">
          <p>Finding Your Location...</p>
        </span>

        <div><p>Graphic/Text</p></div>
      </section>




      

      <section class="info">

        <span class="zip">
          <input id="zipcodeLookup" placeholder="zip code or city, state" />
          <button class="btn-zip">Submit</button>
        </span>

        <div id="location" data-id>
          <p class="title"></p>
          <p class="address"></p>
          <p>
            <span class="city"></span>, 
            <span class="state"></span>
            <span class="zip"></span>
          </p>
          <form id="form" name="form" method="post" action="response.php">
            <input name="email" placeholder="email" />
            <input name="phone" placeholder="phone" />
            <input name="loctitle" type="hidden" />
            <input name="locaddress" type="hidden" />
            <input name="loccity" type="hidden" />
            <input name="locphone" type="hidden" />
            <input name="locwebsite" type="hidden" />
            <input name="locltsid" type="hidden" />
            <input type="submit" value="Submit" id="btn-submit" />
          </form>
        </div>

        <div id="map"></div>

      </section>

      <section class="geoBlockedLocationList"></section>


    <?php include('footer.php') ?>
    <?php include('scripts.php') ?>
    </body>
</html>
